package polymorphism;

public class Ebook extends Book{
    private int numberBytes;

    public Ebook(String title, String author, int numberBytes){
        super(title, author);
        this.numberBytes = numberBytes;
    }
    public int getNumberBytes(){
        return this.numberBytes;
    }
}
