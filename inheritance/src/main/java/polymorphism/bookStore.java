package polymorphism;

public class bookStore {
    public static void main(String[]args){
        Book[] array = new Book[5];
        array[0] = new Book("first", "Fa");
        array[1] = new Ebook("second", "Sa", 22);
        array[2] = new Book("third", "Ta");
        array[3] = new Ebook("fourth", "Foa", 44);
        array[4] = new Ebook("five", "Fia", 55);

        for (int i = 0; i < array.length; i++)
        {
            System.out.println(array[i]);
        }

        Ebook b = (Ebook)array[0];
        System.out.println(b.getNumberBytes());


    }
}
